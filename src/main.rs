use std::collections::{HashMap, HashSet};
use std::error::Error;

extern crate structopt;
use structopt::{
    clap::AppSettings::{ColorAuto, ColoredHelp},
    StructOpt,
};

#[derive(Debug, StructOpt)]
#[structopt(setting(ColorAuto), setting(ColoredHelp), about)]
struct Cli {
    /// Verbose mode (-v, -vv, -vvv, etc.).
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u64,
    /// Number of dimensions to simulate.
    #[structopt(short = "d", long = "number-of-dimensions", default_value = "3")]
    num_dims: u64,
    /// Number of games/iterations to run through before reporting.
    #[structopt(short = "i", long = "number-of-iterations", parse(try_from_str = parse_human_integer), default_value = "1_000_000")]
    iterations: u128,
}

extern crate humannum;
use humannum::Error as HumanNumberError;

fn parse_human_integer(input: &str) -> Result<u128, HumanNumberError> {
    humannum::parse_integer(input)
}

mod game_objects;
use game_objects::{Coordinate, GameBoard};

#[derive(Default, Debug)]
pub struct GameResult {
    coordinates: HashSet<Coordinate>,
    is_game_won: bool,
}

#[allow(clippy::cast_possible_truncation)]
fn main() -> Result<(), Box<dyn Error>> {
    let cli = Cli::from_args();
    let verbosity_level = cli.verbose as usize;
    if 1 < verbosity_level {
        eprint!("\n");
        dbg!(&cli);
    }

    let mut wins: u128 = 0;
    let mut data: HashMap<u128, GameResult> = HashMap::new();
    for game in 1..=cli.iterations {
        let board: GameBoard = GameBoard::new(cli.num_dims, cli.verbose);
        let result: GameResult = GameResult {
            coordinates: board.coordinates(),
            is_game_won: board.is_game_won(),
        };
        if game % 10_u128.pow(6) == 0 || game % (5 * 10_u128.pow(5)) == 0 {
            eprintln!(
                "\tIteration {}: {:#?} -> Won? {}",
                game, &result.coordinates, &result.is_game_won
            );
        }
        wins += if result.is_game_won { 1 } else { 0 };
        data.insert(game, result);
    }
    eprintln!(
        "Total wins: {} out of a total of {} games.",
        wins, cli.iterations
    );
    Ok(())
}
