use std::iter::Iterator;

#[derive(Debug, Clone, Eq, Hash)]
pub struct Coordinate {
    dimensions: Vec<u8>,
}

extern crate rand;
use rand::Rng;

impl<'a> Coordinate {
    pub fn new(coords: &[u8]) -> Self {
        Self {
            dimensions: coords.to_owned(),
        }
    }

    pub fn add(&self, other: &Vector) -> Self {
        Self {
            dimensions: self
                .clone()
                .zip(other)
                .map(|(origin, to)| origin + to)
                .collect(),
        }
    }

    pub fn generate_random_coordinate(number_of_dimensions: u64) -> Self {
        Self {
            dimensions: (0..number_of_dimensions)
                .map(|_| rand::thread_rng().gen_range(0, 3))
                .collect(),
        }
    }

    /*/// Necessary for implementing coordinate.zip(other_coordinate)
    fn values(&self) -> impl Iterator<Item = &u8> {
        self.dimensions.iter()
    }*/
}

/// For being able to insert struct into a `std::collections::HashSet`
impl PartialEq for Coordinate {
    fn eq(&self, other: &Self) -> bool {
        self.dimensions == other.dimensions
    }
}

pub struct IterCoordinate<'a> {
    iter: ::std::slice::Iter<'a, u8>,
}
impl<'a> IntoIterator for &'a Coordinate {
    type Item = &'a u8;
    type IntoIter = IterCoordinate<'a>;

    fn into_iter(self) -> Self::IntoIter {
        IterCoordinate {
            iter: self.dimensions.iter(),
        }
    }
}
impl<'a> Iterator for IterCoordinate<'a> {
    type Item = &'a u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
impl Iterator for Coordinate {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.next()
    }
}

#[derive(Debug, Clone)]
pub struct Vector {
    dimensions: Vec<u8>,
}

impl Vector {
    pub fn new(origin: &Coordinate, destination: &Coordinate) -> Self {
        Self {
            dimensions: origin
                .clone()
                .zip(destination)
                .map(|(from, to)| from - to)
                .collect(),
        }
    }
    pub fn add(&self, other: &Vector) -> Self {
        Self {
            dimensions: self.clone().zip(other).map(|(x, y)| x + y).collect(),
        }
    }
    pub fn subtract(&self, other: &Vector) -> Self {
        Self {
            dimensions: self.clone().zip(other).map(|(x, y)| x - y).collect(),
        }
    }
}

pub struct IterVector<'a> {
    iter: ::std::slice::Iter<'a, u8>,
}
impl<'a> IntoIterator for &'a Vector {
    type Item = &'a u8;
    type IntoIter = IterVector<'a>;

    fn into_iter(self) -> Self::IntoIter {
        IterVector {
            iter: self.dimensions.iter(),
        }
    }
}
impl<'a> Iterator for IterVector<'a> {
    type Item = &'a u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl Iterator for Vector {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.next()
    }
}
