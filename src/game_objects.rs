extern crate rand;

use rand::Rng;
use std::collections::HashSet;

mod structs;
pub use crate::game_objects::structs::{Coordinate, Vector};

#[derive(Debug, Clone)]
pub struct GameBoard {
    number_of_dimensions: u64,
    points: HashSet<Coordinate>,
}

impl<'a> GameBoard {
    pub fn new(number_of_dimensions: u64, _verbosity_level: u64) -> GameBoard {
        let coordinates: HashSet<Coordinate> = HashSet::new();
        let mut board = GameBoard {
            number_of_dimensions,
            points: coordinates.to_owned(),
        };
        board.add_n_random_coordinates(number_of_dimensions);
        board
    }

    pub fn is_game_won(&self) -> bool {
        let coordinates: Vec<Coordinate> = self.points.iter().cloned().collect();
        let vector12: Vector = Vector::new(&coordinates[0], &coordinates[1]);
        unimplemented!();
    }

    pub fn coordinates(&self) -> HashSet<Coordinate> {
        self.points.clone()
    }

    pub fn add_n_random_coordinates(&mut self, num_coords: u64) {
        let mut rng = rand::thread_rng();
        while (self.points.len() as u64) < num_coords {
            let mut coordinate_vector: Vec<u8> =
                Vec::with_capacity(self.number_of_dimensions as usize);
            for _ in 0..self.number_of_dimensions {
                coordinate_vector.push(rng.gen_range(0, 3) as u8);
            }
            let coordinate: Coordinate = Coordinate::new(&coordinate_vector);

            if self.points.contains(&coordinate) {
                continue;
            }
            self.points.insert(coordinate);
        }
    }
}
