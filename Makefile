.PHONY: clippy
clippy:
	touch src/main.rs && cargo clippy --all

.PHONY: clippy_pedantic
clippy_pedantic:
	touch src/main.rs && cargo clippy --all -- -W clippy::pedantic 
